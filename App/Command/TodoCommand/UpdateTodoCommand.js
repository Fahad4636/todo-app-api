const { Command } = require("simple-command-bus");

class UpdateTodoCommand extends Command {
  constructor(todoId, description, completed) {
    super();
    this.todoId = todoId;
    this.description = description;
    this.completed = completed;
  }
}
module.exports = UpdateTodoCommand;
