const { Command } = require("simple-command-bus");

class CreateTodoCommand extends Command {
  constructor(todoId, description, completed, userId) {
    super();
    this.todoId = todoId;
    this.description = description;
    this.completed = completed;
    this.userId = userId;
  }
}
module.exports = CreateTodoCommand;
