const { Command } = require("simple-command-bus");

class FetchTodoByIdCommand extends Command {
  constructor(todoId) {
    super();
    this.todoId = todoId;
  }
}
module.exports = FetchTodoByIdCommand;
