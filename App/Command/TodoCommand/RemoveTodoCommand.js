const { Command } = require("simple-command-bus");

class RemoveTodoCommand extends Command {
  constructor(todoId) {
    super();
    this.todoId = todoId;
  }
}
module.exports = RemoveTodoCommand;
