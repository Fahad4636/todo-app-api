const { Command } = require("simple-command-bus");

class UserLoginCommand extends Command {
  constructor(email, password) {
    super();
    this.email = email;
    this.password = password;
  }
}
module.exports = UserLoginCommand;
