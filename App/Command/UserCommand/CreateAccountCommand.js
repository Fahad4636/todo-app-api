const { Command } = require("simple-command-bus");

class CreateAccountCommand extends Command {
  constructor(userId, userName, email, password) {
    super();
    this.userId = userId;
    this.userName = userName;
    this.email = email;
    this.password = password;
  }
}
module.exports = CreateAccountCommand;
