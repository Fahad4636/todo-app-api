const app = require("./server");

const userRouter = require("../Routes/UserRoutes");
const todoRouter = require("../Routes/TodoRoute");
const apiVersion = "/api/v1";
app.use(`${apiVersion}/user`, userRouter);
app.use(`${apiVersion}/todo`, todoRouter);
module.exports = app;
