const bunyan = require("bunyan");
const log = bunyan.createLogger({
  name: "JWT_Authentication",
  streams: [
    {
      level: "debug",
      stream: process.stdout
    },
    {
      level: "error",
      stream: process.stdout
    }
  ]
});

module.exports = log;
