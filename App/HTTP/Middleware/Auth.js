let jwt = require("jsonwebtoken");
const userModel = require("../../Infrastructure/Models/UserModel");
const serverConfig = require("../../Infrastructure/Config").serverConfig;
const auth = async (req, res, next) => {
  try {
    const token = req.header("x-auth-token");
    const decoded = jwt.verify(token, serverConfig.SECRET);
    console.log("token", decoded);
    const user = await userModel.findOne({
      userId: decoded.userId
    });
    if (!user) {
      throw new Error();
    }
    req.user = user;
    next();
  } catch (e) {
    res.status(401).send({ error: "Please authenticate" });
  }
};
module.exports = auth;
