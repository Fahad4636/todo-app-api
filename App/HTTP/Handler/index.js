const {
  CommandHandlerMiddleware,
  ClassNameExtractor,
  InMemoryLocator,
  HandleInflector
} = require("simple-command-bus");

const CreateAccountHandler = require("./UserHandler/CreateAccountHandler");
const UserLoginHandler = require("./UserHandler/UserLoginHandler");
const CreateTodoHandler = require("./TodoHandler/CreateTodoHandler");
const FetchTodoByIdHandler = require("./TodoHandler/FetchTodoByIdHandler");
const UpdateTodoHandler = require("./TodoHandler/UpdateTodoHandler");
const RemoveTodoHandler = require("./TodoHandler/RemoveTodoHandler");

const commandHanlerMiddleware = new CommandHandlerMiddleware(
  new ClassNameExtractor(),
  new InMemoryLocator({
    CreateAccountHandler: new CreateAccountHandler(),
    UserLoginHandler: new UserLoginHandler(),
    CreateTodoHandler: new CreateTodoHandler(),
    FetchTodoByIdHandler: new FetchTodoByIdHandler(),
    UpdateTodoHandler: new UpdateTodoHandler(),
    RemoveTodoHandler: new RemoveTodoHandler()
  }),
  new HandleInflector()
);
module.exports = commandHanlerMiddleware;
