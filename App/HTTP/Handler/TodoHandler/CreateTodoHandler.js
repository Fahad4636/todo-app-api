const todoService = require("../../../Domain/Services/TodoService");

class CreateTodoHandler {
  async handle(command) {
    return await todoService.addTodo(command);
  }
}
module.exports = CreateTodoHandler;
