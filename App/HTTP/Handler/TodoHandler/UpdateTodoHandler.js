const todoService = require("../../../Domain/Services/TodoService");

class UpdateTodoHandler {
  async handle(command) {
    return await todoService.update(command.todoId, command);
  }
}
module.exports = UpdateTodoHandler;
