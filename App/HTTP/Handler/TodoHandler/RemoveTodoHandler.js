const todoService = require("../../../Domain/Services/TodoService");

class RemoveTodoHandler {
  async handle(command) {
    return await todoService.remove(command.todoId);
  }
}
module.exports = RemoveTodoHandler;
