const todoService = require("../../../Domain/Services/TodoService");

class FetchTodoByIdHandler {
  async handle(command) {
    return await todoService.getById(command.todoId);
  }
}
module.exports = FetchTodoByIdHandler;
