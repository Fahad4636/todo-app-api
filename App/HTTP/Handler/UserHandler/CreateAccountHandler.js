const userService = require("../../../Domain/Services/UserService");

class CreateAccountHandler {
  async handle(command) {
    return await userService.addUser(command);
  }
}
module.exports = CreateAccountHandler;
