const userService = require("../../../Domain/Services/UserService");

class UserLoginHandler {
  async handle(command) {
    return await userService.getByEmail(command.email, command.password);
  }
}
module.exports = UserLoginHandler;
