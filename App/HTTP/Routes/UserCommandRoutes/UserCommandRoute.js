const { CommandBus, LoggerMiddleware } = require("simple-command-bus");
const commandHandlerMiddlware = require("../../Handler");
const logger = require("../../Logger/logger");
const CreateAccountCommand = require("../../../Command/UserCommand/CreateAccountCommand");
const UserLoginCommand = require("../../../Command/UserCommand/UserLoginCommand");
const User = require("../../../Domain/Core/User");

const commandBus = new CommandBus([
  new LoggerMiddleware(console),
  commandHandlerMiddlware
]);

const CreateUserAccount = async (req, res) => {
  try {
    const { userName, email, password } = req.body;
    const user = User.createFromDetails(userName, email, password);
    const createAccountCommand = new CreateAccountCommand(
      user.userId,
      user.userName,
      user.email,
      user.password
    );
    const result = await commandBus.handle(createAccountCommand);
    res.send(result);
  } catch (e) {
    logger.debug(e);
  }
};
const UserLogin = async (req, res) => {
  try {
    const { email, password } = req.body;
    const userLoginCommand = new UserLoginCommand(email, password);
    const result = await commandBus.handle(userLoginCommand);
    res.send(result);
  } catch (e) {
    logger.debug(e);
  }
};
module.exports = { CreateUserAccount, UserLogin };
