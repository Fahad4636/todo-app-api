const express = require("express");
const router = express.Router();

const user = require("./UserCommandRoutes/UserCommandRoute");

router.post("/add", user.CreateUserAccount);
router.post("/login", user.UserLogin);

module.exports = router;
