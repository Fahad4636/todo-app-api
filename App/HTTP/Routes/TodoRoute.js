const express = require("express");
const router = express.Router();
const auth = require("../Middleware/Auth");

const todo = require("./TodoCommandRoutes/TodoCommandRoute");

router.post("/add", auth, todo.createTodo);
router.get("/alltodos", auth, todo.fetchAllTodo);
router.get("/fetchById/:todoId", auth, todo.fetchTodoById);
router.patch("/update/:todoId", auth, todo.updateTodo);
router.delete("/remove/:todoId", auth, todo.removeTodo);
module.exports = router;
