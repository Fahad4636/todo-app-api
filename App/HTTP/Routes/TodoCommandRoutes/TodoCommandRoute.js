const { CommandBus, LoggerMiddleware } = require("simple-command-bus");
const commandHandlerMiddlware = require("../../Handler");
const logger = require("../../Logger/logger");
const CreateTodoCommand = require("../../../Command/TodoCommand/CreateTodoCommand");
const FetchTodoByIdCommand = require("../../../Command/TodoCommand/FetchTodoByIdCommand");
const UpdateTodoCommand = require("../../../Command/TodoCommand/UpdateTodoCommand");
const RemoveTodoCommand = require("../../../Command/TodoCommand/RemoveTodoCommand");
const Todo = require("../../../Domain/Core/Todo");
const commandBus = new CommandBus([
  new LoggerMiddleware(console),
  commandHandlerMiddlware
]);

const createTodo = async (req, res) => {
  try {
    const { description, completed } = req.body;
    const todoAdd = Todo.createFromDetail(
      description,
      completed,
      req.user.userId
    );
    const createTodoCommand = new CreateTodoCommand(
      todoAdd.todoId,
      todoAdd.description,
      todoAdd.completed,
      todoAdd.userId
    );
    const result = await commandBus.handle(createTodoCommand);
    res.send(result);
  } catch (e) {
    logger.debug(e);
  }
};
const fetchAllTodo = async (req, res) => {
  try {
    await req.user
      .populate({
        path: "todos"
      })
      .execPopulate();
    res.send(req.user.todos);
  } catch (e) {
    logger.debug(e);
  }
};
const fetchTodoById = async (req, res) => {
  try {
    const { todoId } = req.params;
    const fetchTodoByIdCommand = new FetchTodoByIdCommand(
      todoId,
      req.user.userId
    );
    const result = await commandBus.handle(fetchTodoByIdCommand);
    res.send(result);
  } catch (e) {
    logger.debug(e);
  }
};
const updateTodo = async (req, res) => {
  try {
    const { todoId } = req.params;
    const { body } = req;
    const updates = Object.keys(body);
    const allowUpdates = ["description", "completed"];
    const isValidOperation = updates.every(update =>
      allowUpdates.includes(update)
    );
    if (!isValidOperation) {
      return { error: "Invalid Updates" };
    }
    const updateTodoCommand = new UpdateTodoCommand(
      todoId,
      body.description,
      body.completed,
      req.user.userId
    );
    const result = await commandBus.handle(updateTodoCommand);
    res.send(result);
  } catch (e) {
    logger.debug(e);
  }
};
const removeTodo = async (req, res) => {
  try {
    const { todoId } = req.params;
    const removeTodoCommand = new RemoveTodoCommand(todoId, req.user.userId);
    const result = await commandBus.handle(removeTodoCommand);
    if (!result) {
      return { error: "Id not found" };
    }
    res.send({ message: "Todo Remove successfully" });
  } catch (e) {
    logger.debug(e);
  }
};
module.exports = {
  createTodo,
  fetchTodoById,
  fetchAllTodo,
  updateTodo,
  removeTodo
};
