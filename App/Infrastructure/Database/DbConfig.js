const mongoose = require("mongoose");
const mongo = require("../Config").databaseConfig;
const logger = require("../../HTTP/Logger/logger");

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${mongo.HOST}:${mongo.PORT}/${mongo.DB}`, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true
});
logger.debug(
  `mongodb://${mongo.HOST}:${mongo.PORT}/${mongo.DB} connected Successfully`
);
