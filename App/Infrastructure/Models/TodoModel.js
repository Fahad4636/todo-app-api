const mongoose = require("mongoose");
const todoSchema = new mongoose.Schema({
  todoId: {
    type: String,
    required: true
  },
  description: {
    type: String,
    trim: true,
    required: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  userId: {
    type: String,
    required: true,
    ref: "user"
  }
});

const todo = mongoose.model("todo", todoSchema);
module.exports = todo;
