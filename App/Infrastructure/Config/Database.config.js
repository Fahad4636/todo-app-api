const env = require("common-env")();

require("dotenv").config();

module.exports = {
  database: {
    mongo: {
      PORT: env.getOrElse(process.env.DB_PORT, "27017"),
      HOST: env.getOrElse(process.env.DB_HOST, "localhost"),
      DB: env.getOrElse(process.env.DB, "todo-app")
    }
  }
};
