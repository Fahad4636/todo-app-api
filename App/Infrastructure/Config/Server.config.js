const env = require("common-env")();
require("dotenv").config();
module.exports = {
  server: {
    PORT: env.getOrElse(process.env.PORT, "5000"),
    APP_NAME: env.getOrElse(process.env.APP_NAME, "Todo-App"),
    SECRET: env.getOrElse(
      process.env.SECRET,
      "m534redfhskjhbvf33769nvdfndb^ff!df@ff"
    ),
    NODE_ENV: env.getOrElse(process.env.NODE_ENV, "development")
  }
};
