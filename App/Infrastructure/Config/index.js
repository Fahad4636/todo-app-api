const databaseConfig = require("./Database.config").database.mongo;
const serverConfig = require("./Server.config").server;

module.exports = {
  databaseConfig,
  serverConfig
};
