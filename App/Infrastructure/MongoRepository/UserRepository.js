const userModel = require("../Models/UserModel");
const User = require("../../Domain/Core/User");

class UserRepository {
  /**
   * add(user) Store in database
   * @param{user} user
   * @returns {Object}
   */

  static async add(user) {
    const add = await userModel.create(user);
    return User.createFromObject(add);
  }

  /**
   * fetchByEmail() Fetch User by email from database
   * @param{email} email
   * @returns {Object}
   */

  static async fetchByEmail(email) {
    const fetchByEmail = await userModel.findOne({ email });
    return User.createFromObject(fetchByEmail);
  }

  /**
   * CheckEmail() Fetch user email to check exisits or not
   * @param{email} email
   * @returns{email}
   */

  static async CheckEmail(email) {
    return await userModel.findOne({ email });
  }
  /**
   * CheckUserName() Fetch user name to check exisits or not
   * @param{userName} userName
   * @returns{userName}
   */

  static async CheckUserName(userName) {
    return await userModel.findOne({ userName });
  }
}

module.exports = UserRepository;
