const todoModel = require("../Models/TodoModel");
const Todo = require("../../Domain/Core/Todo");

class TodoRepository {
  /**
   * add(todo) Store in database
   * @param{todo} todo
   */

  static async add(todo) {
    const add = await todoModel.create(todo);
    return Todo.createFromObject(add);
  }

  /**
   * fetchById(todoId) Fetch todo by id
   * @param {todoId} todoId
   */

  static async fetchById(todoId) {
    const fetchTodoById = await todoModel.findOne({ todoId });
    return Todo.createFromObject(fetchTodoById);
  }

  /**
   * update(todoId,todoObj) Update todo
   * @param {todoId} todoId
   * @param {todoObj} todoObj
   */

  static async update(todoId, todoObj) {
    const todoUpdate = await todoModel.findOneAndUpdate(todoId, todoObj);
    return Todo.createFromObject(todoUpdate);
  }

  /**
   * remove(todoId) Remove todo from Database
   * @param {todoId} todoId
   */

  static async remove(todoId) {
    return await todoModel.findOneAndDelete({ todoId });
  }
}

module.exports = TodoRepository;
