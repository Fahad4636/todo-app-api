const UserRepo = require("../MongoRepository/UserRepository");
const User = require("../../Domain/Core/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const serverConfig = require("../Config/index").serverConfig;

class AuthService {
  static async findByCredentials(email, password) {
    const user = await UserRepo.fetchByEmail(email);
    if (!user) {
      return { error: "Unable to login. Please regiseter user first" };
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return { error: "Password does not match" };
    } else {
      const token = jwt.sign({ userId: user.userId }, serverConfig.SECRET);
      const userObj = User.createFromObject(user);
      return { userObj, token };
    }
  }
}
module.exports = AuthService;
