const uuid = require("uuid/v1");
class Todo {
  constructor(todoId, description, completed, userId) {
    this.todoId = todoId;
    this.description = description;
    this.completed = completed;
    this.userId = userId;
  }

  /**
   * Function will create todo object
   * @param {userObj} todoObj
   */

  static createFromObject(todoObj) {
    return new Todo(
      todoObj.todoId,
      todoObj.description,
      todoObj.completed,
      todoObj.userId
    );
  }

  /**
   * Function will create todo object
   * @param {*} description
   * @param {*} completed
   * @param {*} userId
   */

  static createFromDetail(description, completed, userId) {
    return new Todo(uuid(), description, completed, userId);
  }
}
module.exports = Todo;
