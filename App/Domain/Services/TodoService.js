const todoRepo = require("../../Infrastructure/MongoRepository/TodoRepository");
const logger = require("../../HTTP/Logger/logger");
const Todo = require("../Core/Todo");

class TodoService {
  static async addTodo(todoObj) {
    try {
      return await todoRepo.add(todoObj);
    } catch (e) {
      logger.debug(e);
    }
  }

  static async getById(todoId) {
    try {
      return await todoRepo.fetchById(todoId);
    } catch (e) {
      logger.debug(e);
    }
  }

  static async update(todoId, todoObj) {
    try {
      return await todoRepo.update(todoId, todoObj);
    } catch (e) {
      logger.debug(e);
    }
  }
  static async remove(todoId) {
    try {
      return await todoRepo.remove(todoId);
    } catch (e) {
      logger.debug(e);
    }
  }
}
module.exports = TodoService;
