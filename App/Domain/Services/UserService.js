const userRepo = require("../../Infrastructure/MongoRepository/UserRepository");
const logger = require("../../HTTP/Logger/logger");
const authService = require("../../Infrastructure/Services/Auth");

class UserService {
  /**
   * addUser(userName,email,password) store new user in database
   * using static factory method
   * @param {userName} userName
   * @param {email} email
   * @param {password} password
   * @returns {Object}
   */
  static async addUser(userObj) {
    try {
      let emailCheck = await userRepo.CheckEmail(userObj.email);
      if (emailCheck) {
        return { error: "This email is already exists" };
      }
      let userNameCheck = await userRepo.CheckUserName(userObj.userName);
      if (userNameCheck) {
        return { error: "This username is already exists" };
      }
      return await userRepo.add(userObj);
    } catch (e) {
      logger.debug(e);
    }
  }
  /**
   * getByEmail()get user by email
   * @param {email} email
   * @param {password} password
   */

  static async getByEmail(email, password) {
    try {
      return await authService.findByCredentials(email, password);
    } catch (e) {
      logger.debug(e);
    }
  }
}

module.exports = UserService;
