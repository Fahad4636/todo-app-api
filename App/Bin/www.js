const program = require("commander");
const app = require("../HTTP/Server");
const logger = require("../HTTP/Logger/logger");
const serverConfig = require("../Infrastructure/Config").serverConfig;

require("../Infrastructure/Database/DbConfig");
require("dotenv").config();

program.command("start").action(() => {
  app.listen(serverConfig.PORT, () => {
    logger.debug(
      `${serverConfig.APP_NAME} Listening on port ${serverConfig.PORT}`
    );
  });
});

program.parse(process.argv);
